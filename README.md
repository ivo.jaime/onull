# ONull

Convert pixel images to vector

ONull is an image based Vector Generator for Mac OSX. It allows the user to convert images into rasterized vector graphics.

This tool was developed by Kim Asendorf to give graphic designers the ability to transform small images from the Internet into printable and editable graphics.

ONull is written in Java and uses the Processing core library as graphical engine. It is available for both Mac and via Java for Windows/Linux users.

Sadly the original [onull.net](http://onull.net) website no longer exists, and sadly it's not open source.

[ONull Flickr group](https://www.flickr.com/groups/onull/)

Created by Kim Asendorf. Sadly it's not open source.

## Example Artwork

![onull example 1](https://desbest.com/content/projects/onull/onull00.jpg)

![onull example 2](https://desbest.com/content/projects/onull/onull03-640x479.png)

![onull example 3](https://desbest.com/content/projects/onull/onull06-640x518.jpg)

![onull example 4](https://desbest.com/content/projects/onull/onull10-640x361.jpg)